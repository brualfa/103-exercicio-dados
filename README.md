# Exercício Orientação a Objetos - Jogo de Dados

1- Criar um sistema que simula o funcionamento de um dado de 6 faces. Ao iniciar o programa, deve-se sortear um número aleatoriamente e exibi-lo no console.

2- Aprimorar o sistema para que seja possível utilizar dados com um número customizável de faces. A determinação da quantidade de faces deve ser feito dentro do código fonte.

3- Aprimorar o sistema para que, em cada execução, o dado seja sorteado 3 vezes. O console deve exibir o resultado de cada sorteio e a soma de todos ao fim. 

Ex:
1, 5, 3, 8

4- Aprimorar o sistema para que, em cada execução, o dado seja sorteado em 3 grupos de 3 vezes. 

Ex:

1, 3, 6, 10

6, 4, 6, 16

4, 3, 2, 9

